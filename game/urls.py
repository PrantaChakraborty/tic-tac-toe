from django.urls import path
from .views import IndexView, play_game


app_name = 'game'
urlpatterns = [
    path('', IndexView.as_view(), name='home'),
    path('play/<str:room_code>/<str:choice>/', play_game, name='play_game')
]