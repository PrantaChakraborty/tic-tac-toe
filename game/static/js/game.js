let room_code = document.getElementById('game_board').getAttribute('room_code')
let choice = document.getElementById('game_board').getAttribute('choice')
// console.log(room_code)
// console.log(choice)

// web socket url in game->route.py
let connectionUrl = 'ws://' + window.location.host + '/ws/play/' + room_code + '/';

let gameSocket = new WebSocket(connectionUrl)

let gameBoard = [
    -1, -1, -1,
    -1, -1, -1,
    -1, -1, -1,
]

// index of the win matrix
winIndices = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
]

let moveCount = 0
let myTurn = true

// click event listener on every block

let elementArray = document.getElementsByClassName('square')
for (let i =0; i< elementArray.length; i++){
    elementArray[i].addEventListener('click', event =>{
        const index = event.composedPath()[0].getAttribute('data-index')
        if(gameBoard[index] === -1){
            if(!myTurn){
                alert('Wait for other to place the move')
            }
            else{
                myTurn = false
                document.getElementById('alert_move').style.display = 'none'
                make_move(index, choice)
            }
        }
    })
}

function make_move(index, player){
    index = parseInt(index)
    let data = {
        "event": "MOVE",
        "message": {
            "index": index,
            "player": player
        }
    }
    // console.log(JSON.stringify(data))

    if(gameBoard[index] === -1){
        // if the valid move, update the gameboard
        // state and send the move to the server.
        moveCount++
        // console.log('move count', moveCount)
        if(player==='X'){
            gameBoard[index] = 1;
        }
        else if(player === 'O'){
            gameBoard[index] = 0
        }
        else{
            alert("Invalid character choice");
            return false;
        }

        // printing the gameboard after making moves
        // for(let i=0; i<gameBoard.length; i++){
        //     console.log('game board', gameBoard[i])
        // }
        gameSocket.send(JSON.stringify(data))
    }
    // place the move in the game box
    elementArray[index].innerHTML = player
    const win = checkWinner()
    if(myTurn){
        if(win){
            data = {
                "event": "END",
                "message": `${player} win the game. Play again?`
            }
            gameSocket.send(JSON.stringify(data))
        }
        else if(!win && moveCount === 9){
            data = {
                "event": "END",
                "message": "It's a draw. Play again?"
            }
            gameSocket.send(JSON.stringify(data))
        }
    }
}

function reset(){
    gameBoard = [
        -1, -1, -1,
        -1, -1, -1,
        -1, -1, -1,
    ];
    moveCount = 0
    myTurn = true
    document.getElementById('alert_move').style.display = 'inline'
    for (let i = 0; i<elementArray.length; i++){
        elementArray[i].innerHTML = ""
    }
}

// check if their is winning move

// checking the index of X or O which match the subarray from winIndices
const check = (winIndex) => {
    // console.log('g borad', gameBoard[winIndex[0]])
   if(gameBoard[winIndex[0]] !== -1 &&
        gameBoard[winIndex[0]] === gameBoard[winIndex[1]] &&
        gameBoard[winIndex[0]] === gameBoard[winIndex[2]]) return true

}

// for checking who is winner
function checkWinner(){
    let win = false
    // console.log('chek winner move count', moveCount)
    if (moveCount >=5){
        winIndices.forEach(w =>{
            // console.log('win index w is', w)
            if(check(w)){
                win=true
                //winIndices = w
            }
        })
    }
    return win
}

// Main function which handles the connection
// of websocket.

function connect(){
    gameSocket.onopen = function open(){
        // console.log('web socket connection created')
        gameSocket.send(JSON.stringify({
            "event": "START",
            "message": ""
        }))
    }

    gameSocket.onclose = function (e){
        console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
        setTimeout(function (){
            connect()
        }, 1000)
    }

    gameSocket.onmessage = function (e){
        let data = JSON.parse(e.data)
        data = data["payload"]
        let message = data["message"]
        let event = data["event"]
        switch (event){
            case 'START':
                reset();
                break;
            case 'END':
                alert(message)
                reset()
                break;
            case 'MOVE':
                if(message.player !== choice){
                    make_move(message['index'], message['player'])
                    myTurn = true
                    document.getElementById('alert_move').style.display = 'inline'
                }
                break;
            default:
                console.log('no event')
        }
    }
    if (gameSocket.readyState === WebSocket.OPEN){
        gameSocket.onopen()
    }
}

// call the connect function to start
connect()