import json
from channels.generic.websocket import AsyncJsonWebsocketConsumer


class TicTacToeConsumer(AsyncJsonWebsocketConsumer):

    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['room_code']
        self.room_group_name = f"room_{self.room_name}"
        # join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    async def disconnect(self, code):
        print('disconnected')

        # leave the group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    async def receive_json(self, data, **kwargs):
        data = json.dumps(data)  # make to data in json format by replacing single quote to double quote
        response = json.loads(data)
        event = response.get('event', None)
        message = response.get('message', None)
        if event == 'MOVE':
            # Send message to room group
            await self.channel_layer.group_send(self.room_group_name, {
                "type": "send_message",
                "message": message,
                "event": "MOVE"
            })

        if event == 'START':
            await self.channel_layer.group_send(self.room_group_name, {
                "type": "send_message",
                "message": message,
                "event": "START"
            })
        if event == 'END':
            await self.channel_layer.group_send(self.room_group_name, {
                "type": "send_message",
                "message": message,
                "event": "END"
            })

    async def send_message(self, res):
        """
        receive message from room group
        """
        await self.send(text_data=json.dumps({
            "payload": res
        }))


