from django.shortcuts import render, redirect, Http404
from django.views.generic import View

# Create your views here.


class IndexView(View):
    def get(self, *args, **kwargs):
        return render(self.request, 'index.html', {})

    def post(self,  *args, **kwargs):
        if self.request.method == 'POST':
            room_code = self.request.POST.get('room_code')
            choice = self.request.POST.get('choice')
            return redirect('game:play_game', room_code, choice)
        return render(self.request, "index.html", {})


def play_game(request, room_code, choice):
    if choice not in ['X', 'O']:
        raise Http404("Choice doesn't exist")
    context = {
        'room_code': room_code,
        'choice': choice
    }
    return render(request, 'game.html', context)



